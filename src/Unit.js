export const type = {
  LENGTH: "Length",
  WEIGHT: "Weight",
  TEMPERATURE: "Temperature"
}

export const unit = {
  CENTIMETRE: new Unit(1, type.LENGTH, 0),
  METRE: new Unit(100, type.LENGTH,0),
  KILOMETRE: new Unit(100000, type.LENGTH,0),
  GRAM: new Unit(1, type.WEIGHT,0),
  KILOGRAM: new Unit(1000, type.WEIGHT,0),
  CELSIUS: new Unit(9, type.TEMPERATURE, 32),
  FAHRENHEIT: new Unit(5, type.TEMPERATURE, -17.77)
}

function Unit(factor, type, constant){
  this.factor = factor;
  this.type = type;
  this.constant = constant;

  this.convertTo = (value, unit) => {
    return Math.round(this.factor / unit.factor * value + constant);
  }

  this.compareUnit = (unit) => {
    if (this.type === unit.type) {
      return true;
    }
    throw Error('Incompatible Types Exception')
  }
}
