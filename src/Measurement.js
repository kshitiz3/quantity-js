export class Measurement {
  constructor(value, unit) {
    this.value = value;
    this.unit = unit;
  }

  equals(anotherMeasurement) {
    return this.unit.compareUnit(anotherMeasurement.unit) && (this.unit.convertTo(this.value, anotherMeasurement.unit) === anotherMeasurement.value);
  }

  add(anotherMeasurement){
    this.unit.compareUnit(anotherMeasurement.unit);

    const newAddedValue = this.value + anotherMeasurement.unit.convertTo(anotherMeasurement.value, this.unit);

    return new Measurement(newAddedValue, this.unit);
  }

  subtract(anotherMeasurement) {
    return this.add(new Measurement(anotherMeasurement.value * -1, anotherMeasurement.unit))
  }
}
