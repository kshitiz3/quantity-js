import {Measurement} from "../src/Measurement.js";
import {unit} from "../src/Unit";

describe("Measurement", () => {

  describe("Operations", () => {

    test("add should return 200 centimetres when 100 centimetre and 1 metre are"
        + " added", () => {
      const measurement = new Measurement(100, unit.CENTIMETRE);
      const anotherMeasurement = new Measurement(1, unit.METRE);
      const expectedMeasurement = new Measurement(200, unit.CENTIMETRE);

      const actualMeasurement = measurement.add(anotherMeasurement);

      expect(actualMeasurement.equals(expectedMeasurement)).toEqual(true);
    })

    test("add should return 0.9 centimetres when 1 kilogram and 100 grams are"
        + " subtracted", () => {
      const measurement = new Measurement(1, unit.KILOGRAM);
      const anotherMeasurement = new Measurement(100, unit.GRAM);
      const expectedMeasurement = new Measurement(0.9, unit.KILOGRAM);

      const actualMeasurement = measurement.subtract(anotherMeasurement);

      expect(actualMeasurement.equals(expectedMeasurement)).toEqual(true);
    })

    test("add should throw error when 1 kilogram and 100 metres are"
        + " subtracted", () => {
      const measurement = new Measurement(1, unit.KILOGRAM);
      const anotherMeasurement = new Measurement(100, unit.METRE);

      expect(() => {
        measurement.add(anotherMeasurement)
      }).toThrow(Error('Incompatible Types Exception'));

    })
  })
})

