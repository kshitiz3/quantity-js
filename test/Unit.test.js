import {Measurement} from "../src/Measurement";
import {unit} from "../src/Unit";

describe("Unit", () => {

  describe("Equals", () => {
    test('equals should return true when 1 centimetre and 1 centimetre are'
        + ' compared', () => {
      const measurement = new Measurement(1, unit.CENTIMETRE);
      const anotherMeasurement = new Measurement(1, unit.CENTIMETRE);

      expect(measurement.equals(anotherMeasurement)).toEqual(true);
    })

    test("equals should return true when 100 cm and 1 metre are compared",
        () => {
          const measurement = new Measurement(100, unit.CENTIMETRE);
          const anotherMeasurement = new Measurement(1, unit.METRE);

          expect(measurement.equals(anotherMeasurement)).toEqual(true);

        })

    test("equals should return true when 1000 metres and 1 kilometre are"
        + " compared", () => {
      const measurement = new Measurement(1000, unit.METRE);
      const anotherMeasurement = new Measurement(1, unit.KILOMETRE);

      expect(measurement.equals(anotherMeasurement)).toEqual(true);
    })

    test("equals should return true when 1 kilometre and 1000 metres are"
        + " compared", () => {
      const measurement = new Measurement(1, unit.KILOMETRE);
      const anotherMeasurement = new Measurement(1000, unit.METRE);

      expect(measurement.equals(anotherMeasurement)).toEqual(true);
    })

    test("equals should return true when 1000 grams is compared to 1 kilogram",
        () => {
          const measurement = new Measurement(1000, unit.GRAM);
          const anotherMeasurement = new Measurement(1, unit.KILOGRAM);

          expect(measurement.equals(anotherMeasurement)).toEqual(true);
        })

    test("equals should return true when 1.1 kilograms is compared to 1100"
        + " grams",
        () => {
          const measurement = new Measurement(1.1, unit.KILOGRAM);
          const anotherMeasurement = new Measurement(1100, unit.GRAM);

          expect(measurement.equals(anotherMeasurement)).toEqual(true);
        })

    test("equals should return false when 1 gram is compared to 1 centimetre",
        () => {
          const measurement = new Measurement(1, unit.GRAM);
          const anotherMeasurement = new Measurement(1, unit.CENTIMETRE);

          expect(() => {
            measurement.equals(anotherMeasurement)
          }).toThrow(Error('Incompatible Types Exception'));
        })
    test('should return false when 1 celsius is compared to 1 gram', () => {
      const measurement = new Measurement(1, unit.CELSIUS);
      const anotherMeasurement = new Measurement(1, unit.CENTIMETRE);

      expect(() => {
        measurement.equals(anotherMeasurement)
      }).toThrow(Error('Incompatible Types Exception'));
    })
    test('should return false when 1 fahrenheit is compared to 1 centimetre',
        () => {
          const measurement = new Measurement(1, unit.FAHRENHEIT);
          const anotherMeasurement = new Measurement(1, unit.CENTIMETRE);

          expect(() => {
            measurement.equals(anotherMeasurement)
          }).toThrow(Error('Incompatible Types Exception'));
        })

    test('should return true when 212 fahrenheit is compared to celsius',
        () => {
          const measurement = new Measurement(212, unit.FAHRENHEIT);
          const anotherMeasurement = new Measurement(100, unit.CELSIUS);

          expect(measurement.equals(anotherMeasurement)).toEqual(true);
        })
  })
})
